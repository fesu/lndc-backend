package com.lndc.website.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * NewsEntry
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-27T23:48:56.155306500+01:00[Europe/Berlin]")

public class NewsEntry   {

  private static int newsCounter = 0;

  @JsonProperty("date")
  private OffsetDateTime date = OffsetDateTime.now();

  @JsonProperty("author")
  private User author;

  @JsonProperty("content")
  private String content;

  @JsonProperty("newsId")
  private String newsId = "" + newsCounter++;

  public NewsEntry date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @ApiModelProperty(required = true, readOnly = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  public NewsEntry author(User author) {
    this.author = author;
    return this;
  }

  /**
   * Get author
   * @return author
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public User getAuthor() {
    return author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }

  public NewsEntry content(String content) {
    this.content = content;
    return this;
  }

  /**
   * Get content
   * @return content
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public NewsEntry newsId(String newsId) {
    this.newsId = newsId;
    return this;
  }

  /**
   * Get newsId
   * @return newsId
  */
  @ApiModelProperty(required = true, readOnly = true, value = "")
  @NotNull


  public String getNewsId() {
    return newsId;
  }

  public void setNewsId(String newsId) {
    this.newsId = newsId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NewsEntry newsEntry = (NewsEntry) o;
    return Objects.equals(this.date, newsEntry.date) &&
        Objects.equals(this.author, newsEntry.author) &&
        Objects.equals(this.content, newsEntry.content) &&
        Objects.equals(this.newsId, newsEntry.newsId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, author, content, newsId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NewsEntry {\n");

    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    content: ").append(toIndentedString(content)).append("\n");
    sb.append("    newsId: ").append(toIndentedString(newsId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

