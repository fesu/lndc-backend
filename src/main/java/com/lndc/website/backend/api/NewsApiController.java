package com.lndc.website.backend.api;

import com.lndc.website.backend.model.NewsEntry;
import com.lndc.website.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-27T23:33:54.282024+01:00[Europe/Berlin]")

@Controller
@RequestMapping("${openapi.lNDCOpen.base-path:/v1}")
public class NewsApiController implements NewsApi {

    private final NativeWebRequest request;
    private final List<NewsEntry> newsRepo = new LinkedList<>();

    @Autowired
    private UsersApiController usersApiController;

    @Autowired
    public NewsApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Void> createNews(@NotNull @Valid String content, @NotNull @Valid String authorName) {
        ResponseEntity<User> authorResponse = usersApiController.getUser(authorName); // FIXME there should be a service
        User author = authorResponse.getBody();
        NewsEntry newsEntry = new NewsEntry().author(author).content(content);
        newsRepo.add(newsEntry);
        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<List<NewsEntry>> getAllNews() {
        return ResponseEntity.ok(Collections.unmodifiableList(newsRepo));
    }

    @Override
    public ResponseEntity<NewsEntry> getNews(String newsId) {
        if (!StringUtils.isEmpty(newsId)) {
            Optional<NewsEntry> newsEntry = newsRepo.stream().filter(n -> n.getNewsId().equals(newsId)).findAny();
            if (newsEntry.isPresent()) {
                return ResponseEntity.ok(newsEntry.get());
            }
        }
        return (ResponseEntity<NewsEntry>) ResponseEntity.badRequest();
    }
}
