package com.lndc.website.backend.api;

import com.lndc.website.backend.model.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.*;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-25T22:15:41.953677200+01:00[Europe/Berlin]")

@Controller
@RequestMapping("${openapi.swaggerPetstore.base-path:/v1}")
public class UsersApiController implements UsersApi {

    private final NativeWebRequest request;

    private final List<User> userRepo = new LinkedList<>();

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(NativeWebRequest request) {
        this.request = request;
        User user = new User();
        user.setId(JsonNullable.of(UUID.randomUUID()));
        user.setCreationDate(JsonNullable.of(OffsetDateTime.now()));
        user.setFirstName("Ad");
        user.setLastName("Min");
        user.setUserName("admin");
        userRepo.add(user);
    }

    @Override
    public ResponseEntity<User> getUser(String userName) {
        if (! StringUtils.isEmpty(userName)) {
            Optional<User> user = userRepo.stream().filter(u -> u.getUserName().equals(userName)).findAny();
            if (user.isPresent()) {
                return ResponseEntity.ok(user.get());
            }
        }
        return (ResponseEntity<User>) ResponseEntity.badRequest();
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<User> createUser(@NotNull @Valid String userName, @Valid String firstName, @Valid String lastName) {
        if (userRepo.stream().noneMatch(u -> u.getUserName().equals(userName))) {
            User user = new User();
            user.setId(JsonNullable.of(UUID.randomUUID()));
            user.setCreationDate(JsonNullable.of(OffsetDateTime.now()));
            user.setUserName(userName);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            userRepo.add(user);
            return ResponseEntity.ok(user);
        }
        return (ResponseEntity<User>) ResponseEntity.badRequest();
    }

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(Collections.unmodifiableList(userRepo));
    }
}
